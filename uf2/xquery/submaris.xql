<html>
    <head>
        <link rel="stylesheet" href="estil.css"></link>
        <img src="TheExpanseWiki-S4-title.png"></img>
    </head>
    <table border="1">
        <tr>
            <xsl:variable 
                name="martian"
                select="count(the_expanse/Martian_Congressional_Republic_Navy/nau)">
            </xsl:variable>
            <xsl:variable 
                name="united"
                select="count(the_expanse/United_Nations_Navy/nau)">
            </xsl:variable>
            <td>Nº de naus: <xsl:value-of select="sum(martian/united)"/></td>