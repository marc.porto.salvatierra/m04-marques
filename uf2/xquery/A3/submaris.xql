let $activitat3_1.html := doc("submaris.xml")
return 
<html>
  <head>
    <link rel="stylesheet" href="./estil_SOLUCIO.css"/>
  </head>
  <body>
    <table border="0">
      <tr>
        {let $nom:= doc("submaris.xml")/submarinos/titulo
         return <th>{$nom}</th>}
      </tr>
      <tr>
        {let $logo:=doc("submaris.xml")/submarinos/logo
        return <th><img src="./banderes/{$logo}"/></th>}
      </tr>
    </table>
    
    <table border="1" id="t01">
      <tr>
        <th >Nom</th>
        <th class="normal" style="width:410px">Propulsió</th>
        <th class="normal" style="width:220px">Torpedes</th>
        <th class="normal" style="width:250px">Missils</th>
      </tr>
      {for $submarino in doc("submaris.xml")/submarinos/submarino
       let $propulsion:=$submarino/caracteristicas_generales/propulsion
       let $potencia_cv:=$submarino/caracteristicas_generales/potencia_caballos
      return 
        <tr>
          <td> <img src="./imatges/{$submarino/imatges/foto}" style="width:256px"/> </td>
          <td> {data($propulsion)} ({(data($potencia_cv))})</td>
          <td> {for $torpedo in doc("submaris.xml")/submarinos/submarino/armamento/armas/torpedos/torpedo_identificador
                return $torpedo/nombre}</td>
          <td></td>
        </tr>}
    </table>
  </body>
</html>