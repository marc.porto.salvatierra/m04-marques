<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<html>
    <body>
        <center><table border="1">
            <tr>
                <td colspan="4" style=" text-align:center" bgcolor="grey">Comanda</td>
            </tr>
            <tr bgcolor="grey">
                <th>Nom</th>
                <th>Adreça</th>
                <th>Ciutat</th>
                <th>C.P.</th>
            </tr>
            <xsl:for-each select="Pedido/Destino">
            <tr style="text-align: center">
                <th bgcolor="blue"><xsl:value-of select="Nombre"/></th>
                <td><xsl:value-of select="Direccion"/></td>
                <td><xsl:value-of select="Ciudad"/></td>
                <td><xsl:value-of select="CodPostal"/></td>
            </tr>
        </xsl:for-each>
        <tr>
            <td colspan="4"><xsl:text>&#160;</xsl:text></td>
        </tr>
        <tr>
            <td colspan="4" style=" text-align:center">Llista amb "Precio &gt; 25" i "Precio &lt;= 100"</td>
        </tr>
        <tr>
            <th colspan="2">Producte</th>
            <th>Preu</th>
            <th>Quantitat</th>
        </tr>
        <xsl:for-each select="Pedido/Contenido/Producto">
            <xsl:sort select="Precio"/>
            <xsl:choose>
                <xsl:when test="Precio &#62; 25 and Precio &#60; 50">
                    <tr>
                        <td colspan="2"><xsl:value-of select="Nombre"/></td>
                        <td style="text-align: center"><xsl:value-of select="Precio"/></td>
                        <td bgcolor="yellow" style="text-align: center"><xsl:value-of select="Cantidad"/></td>
                    </tr>
                </xsl:when>
                <xsl:when test="Precio &#62; 50 and Precio &#60; 75">
                    <tr>
                        <td colspan="2"><xsl:value-of select="Nombre"/></td>
                        <td style="text-align: center"><xsl:value-of select="Precio"/></td>
                        <td bgcolor="green" style="text-align: center"><xsl:value-of select="Cantidad"/></td>
                    </tr>
                </xsl:when>
                <xsl:when test="Precio &#62; 75 and Precio &#60; 100">
                    <tr>
                        <td colspan="2"><xsl:value-of select="Nombre"/></td>
                        <td style="text-align: center"><xsl:value-of select="Precio"/></td>
                        <td bgcolor="red" style="text-align: center"><xsl:value-of select="Cantidad"/></td>
                    </tr>
                </xsl:when>
            </xsl:choose>
        </xsl:for-each>
        </table></center> 
    </body>
</html>
</xsl:template>
</xsl:stylesheet>