<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<html>
    <head>
        <link rel="stylesheet" href="estil.css"></link>
        <img src="TheExpanseWiki-S4-title.png"></img>
    </head>
    <table border="1">
        <tr>
            <xsl:variable 
                name="martian"
                select="count(the_expanse/Martian_Congressional_Republic_Navy/nau)">
            </xsl:variable>
            <xsl:variable 
                name="united"
                select="count(the_expanse/United_Nations_Navy/nau)">
            </xsl:variable>
            <td>Nº de naus: <xsl:value-of select="sum(martian/united)"/></td>
        </tr>
    </table>    
</html>
</xsl:template>
</xsl:stylesheet>