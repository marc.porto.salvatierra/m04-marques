<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<html>
    <head>
        <link rel="stylesheet" href="estil.css"></link>
    </head>
    <body>
        <h2 bgcolor="grey"><xsl:value-of select="root/origen/productor"/></h2>
        <a>
            <xsl:attribute name="href">
                <xsl:value-of select="root/origen/web"/>
            </xsl:attribute>
            <xsl:value-of select="root/origen/web"/>
        </a>
        <p>Dades per a: <font class="letter"><xsl:value-of select="/root/nombre"/> - <xsl:value-of select="root/provincia"/></font></p>
        <table border="1">
            <xsl:for-each select="root/prediccion/dia"/>
            <xsl:sort select="@fecha"/>
            <tr>
                <td colspan="3">Data: <font class="bg"><xsl:value-of select="@fecha"/></font></td>
            </tr>
            <tr class="yellow">
                <td colspan="3">Probabilitat precipitacio</td>
            </tr>
            <xsl:for-each select="prob_precipitacion">
                <tr>
                    <td><xsl:value-of select="@periodo"/></td>
                    <td colspan="2"><xsl:value-of select="."/></td>
                </tr>
            </xsl:for-each>
            <tr class="yellow">
                <td colspan="3">Dades del vent</td>
            </tr>
            <tr class="grey">
                <th>Periode</th>
                <th>DIreccio</th>
                <th>Velocitat</th>
            </tr>
            <xsl:for-each select="viento">
                <tr>
                    <td><xsl:value-of select="@periodo"/></td>
                    <td><xsl:value-of select="direccion"/></td>
                    <td><xsl:value-of select="velocidad"/></td>
                </tr>
            <tr>
                <th>Quantitat de refistres vent</th>
                <th>Suma velocitats</th>
                <th>Mitjana velocitat</th>
            </tr>
            <tr>
                <td><xsl:value-of select="count(viento)"/></td>
                <td><xsl:value-of select="sum(viento/velocidad)"/></td>
                <td><xsl:value-of select="sum(viento/velocidad) div count(viento)"/></td>
            </tr>
            <tr class="grey">
                <th colspan="3">Temperatura/UV</th>
            </tr>
            <tr class="grey">
                <th>Temp Maxima</th>
                <th>Temp Minima</th>
                <th>UV</th>
            </tr>
            <tr>
                <td><xsl:value-of select="temperatura/maxima"/></td>
                <td><xsl:value-of select="temperatura/minima"/></td>
                <td><xsl:value-of select="uv_max"/></td>
            </tr>
            </xsl:for-each>
        </table>
    </body>
</html>
</xsl:template>
</xsl:stylesheet>