<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<html>
    <title>Activitat 1 XSLT</title>
    <body>
        <center><table border="1">
            <tr>
                <th></th>
                <th>Classificació</th>
                <th></th>
            </tr>
            <tr bgcolor="#9acd32">
                <th>Plataforma del programa</th>
                <th>Nom</th>
                <th>Tipus de llicència</th>
            </tr>
            <xsl:for-each select="programes/editors_XML/programa">
                <tr>
                    <td><xsl:value-of select="plataforma"/></td>
                    <td bgcolor="red" style=" text-align:center"><xsl:value-of select="nom"/></td>
                    <td><xsl:value-of select="llicencia"/></td>
                </tr>
            </xsl:for-each>
        </table></center>
    </body>
</html>
</xsl:template>
</xsl:stylesheet>